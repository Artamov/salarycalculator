<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_salary', function (Blueprint $table) {
            $table->id();
            $table->integer('salary_amount');
            $table->integer('salary_take');
            $table->integer('ipnTax');
            $table->integer('opvTax');
            $table->integer('vocmcTax');
            $table->integer('ocmcTax');
            $table->integer('coTax');
            $table->integer('correct');
            $table->integer('salary');
            $table->integer('days_of_work');
            $table->boolean('is_tax_deduction');
            $table->string('year');
            $table->string('month');
            $table->boolean('is_retiree');
            $table->boolean('is_disabled');
            $table->tinyInteger('disabled_group');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_salary');
    }
}
