<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Salary\StoreRequest;
use App\Services\SalaryCalculate;
use Illuminate\Http\Response;

class SalaryCalculateController extends Controller
{
    public function calculate(StoreRequest $request, SalaryCalculate $salaryCalculate)
    {
        return response(
            [$salaryCalculate->calculate()],
            Response::HTTP_OK
        ); // TODO JSON Collection
    }

    public function calculateSave(StoreRequest $request, SalaryCalculate $salaryCalculate)
    {
        return response(
            [$salaryCalculate->calculateSave()],
            Response::HTTP_OK
        ); // TODO JSON Collection
    }
}
