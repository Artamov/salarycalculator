<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    use HasFactory;

    protected $table = 'users_salary';

    protected $fillable = [
        'salary',
        'salary_take',
        'salary_amount',
        'ipnTax',
        'opvTax',
        'vocmcTax',
        'ocmcTax',
        'coTax',
        'correct',
        'days_of_work',
        'is_tax_deduction' ,
        'year',
        'month',
        'is_retiree',
        'is_disabled',
        'disabled_group'
    ];


    public static $rules = [
        'salary'            => ['required', 'integer'],
        'days_of_work'      => ['required', 'integer'],
        'is_tax_deduction'  => ['required', 'boolean'],
        'year'              => ['required', 'date_format:Y'],
        'month'             => ['required', 'date_format:m'],
        'is_retiree'        => ['required', 'boolean'],
        'is_disabled'       => ['required', 'boolean'],
        'disabled_group'    => ['exclude_if:is_disabled,false', 'required', 'integer','between:1,3']
    ];


}
