<?php


namespace App\Services;


use App\Models\Salary;

class SalaryCalculate
{

    public $salary_amount;      // начислено
    public $salary_take;        // на руки

    public $ipnTax;
    public $opvTax;
    public $vocmcTax;
    public $ocmcTax;
    public $coTax;
    public $correct;

    public $salary;             // оклад
    public $days_of_work;       // отработано дней
    public $is_tax_deduction;   // облагается ли налогом
    public $year;
    public $month;
    public $is_retiree;         // пенсионер
    public $is_disabled;        // инвалид
    public $disabled_group;

    public function __construct(array $salaryData)
    {

        $this->salary = $salaryData['salary'];
        $this->days_of_work = $salaryData['days_of_work'];
        $this->is_tax_deduction = $salaryData['is_tax_deduction'];
        $this->year = $salaryData['year'];
        $this->month = intval($salaryData['month']);
        $this->is_retiree = $salaryData['is_retiree'];
        $this->is_disabled = $salaryData['is_disabled'];
        $this->disabled_group = $salaryData['disabled_group'] ?? null;

    }

    public function init()
    {
        $this->calculateOPV();
        $this->calculateVOCMC();
        $this->calculateOCMC();
        $this->calculateCO();
        $this->calculateCorrect();
        $this->calculateIPN();
        $this->calculateSalaryAmount();
    }


    //TODO  в онлайн калькуляторах используется еще подсчет с СН

    public function calculate()
    {
        $this->init();

        if($this->is_retiree && $this->is_disabled) {
            $this->calculateRetireeDisabledEployeeSalary();
        }
        else if($this->is_retiree) {
            $this->calculateRetireeEployeeSalary();
        }
        else if($this->is_disabled) {
            if($this->disabled_group === 3) {
                $this->calculateDisabledThreeGroupEployeeSalary();
            }else {
                $this->calculateOtherDisabledGroupEployeeSalary();
            }
        }else {
            $this->calculateNormalЕmployeeSalary();
        }

        return $this;
    }

    public function calculateSave()
    {
        $salaryData = $this->calculate();

        return Salary::create((array)$salaryData);
    }

    public function calculateIPN()
    {
        $this->ipnTax = intval((($this->salary - $this->opvTax - ($this->is_tax_deduction ?
                \Tax::$salary['MZP'] : 0) - $this->vocmcTax - ($this->is_tax_deduction ?
                $this->correct : 0)) / 100) * \Tax::$salary['IPN']);
    }

    public function calculateOPV()
     {
        $this->opvTax = intval(($this->salary / 100) * \Tax::$salary['OPV']);

    }

    public function calculateVOCMC()
    {
        $this->vocmcTax =  intval(($this->salary / 100) * \Tax::$salary['VOCMC']);

    }

    public function calculateOCMC()
    {
        $this->ocmcTax = intval(($this->salary / 100) * \Tax::$salary['OCMC']);

    }

    public function calculateCO()
    {
        $this->coTax = intval((($this->salary - $this->opvTax) / 100) * \Tax::$salary['CO']);
    }

    public function calculateCorrect()
    {
        return $this->correct = intval((($this->salary - $this->opvTax -  ($this->is_tax_deduction ?
            \Tax::$salary['MZP'] : 0) - $this->vocmcTax) / 100) * \Tax::$salary['CORRECT']);
    }

    public function calculateNormalЕmployeeSalary()
    {
        $this->salary_take = intval(((($this->salary - $this->opvTax - $this->ipnTax))
        / $this->monthWorkingDayCount()) * $this->days_of_work);
        $this->checkCorrect();
    }

    public function calculateRetireeEployeeSalary()
    {
        $this->salary_take = intval(((($this->salary - $this->ipnTax))
                / $this->monthWorkingDayCount()) * $this->days_of_work);
        $this->checkCorrect();
    }

    public function calculateDisabledThreeGroupEployeeSalary()
    {
        $salary = intval(((($this->salary - $this->opvTax - $this->coTax))
                / $this->monthWorkingDayCount()) * $this->days_of_work);

        if($salary > \Tax::$salary['DISABLED_LIMIT'] * \Tax::$salary['MRP']) {
            $this->salary_take = $salary - $this->ipnTax();
        }
        $this->salary_take = $salary;
        $this->checkCorrect();
    }

    public function calculateOtherDisabledGroupEployeeSalary()
    {
        $this->salary_take = intval(((($this->salary - $this->coTax))
                / $this->monthWorkingDayCount()) * $this->days_of_work);
        $this->checkCorrect();
    }

    public function calculateRetireeDisabledEployeeSalary()
    {
        $this->salary_take =($this->salary
                / $this->monthWorkingDayCount()) * $this->days_of_work;
        $this->checkCorrect();
    }

    public function calculateSalaryAmount()
    {
        $this->salary_amount = intval(($this->salary
            / $this->monthWorkingDayCount()) * $this->days_of_work);
    }

    public function checkCorrect()
    {
        if($this->salary_take < (\Tax::$salary['MRP'] * \Tax::$salary['CORRECT_MRP'])) {
            $this->salary_take = $this->calculateCorrect();
        }

    }


    public function monthWorkingDayCount()
    {
        return $this->countDays($this->year, $this->month, [0,6]);
    }

     // подсчет кол-во рабочих дней в году
     public function countDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count;
    }




}
