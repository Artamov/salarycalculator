<?php

namespace App\Providers;

use App\Http\Requests\Salary\StoreRequest;
use App\Services\SalaryCalculate;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class SalaryCalculateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */


    protected $request;


    public function register()
    {
        $this->app->bind('App\Services\SalaryCalculate', function ($app) {
            return new SalaryCalculate($this->request);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(StoreRequest $request)
    {
        $this->request = $request->validated();
    }
}
