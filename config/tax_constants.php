<?php

class Tax {
    public static $salary = [
        'IPN'               => 10,
        'OPV'               => 10,
        'CORRECT'           => 90,
        'VOCMC'             => 2,
        'OCMC'              => 2,
        'CO'                => 3.5,
        'DISABLED_LIMIT'    => 882,
        'MRP'               => 2917,
        'MZP'               => 42500,
        'CORRECT_MRP'       => 25
    ];
}
